# [3.0.0](https://gitlab.com/alline/hook-wiki/compare/v2.1.0...v3.0.0) (2020-07-09)


### Bug Fixes

* update dependencies ([1c72b6f](https://gitlab.com/alline/hook-wiki/commit/1c72b6f28997d857552dac6105b73829570156d9))


### BREAKING CHANGES

* update @alline/core to v3

# [2.1.0](https://gitlab.com/alline/hook-wiki/compare/v2.0.0...v2.1.0) (2020-07-02)


### Features

* add validateText ([4eb20c7](https://gitlab.com/alline/hook-wiki/commit/4eb20c7f33c54a2c5f49e2a0b3269ef8ab41eb91))

# [2.0.0](https://gitlab.com/alline/hook-wiki/compare/v1.1.2...v2.0.0) (2020-07-02)


### Features

* support @alline/scraper-html v2 ([6b0f522](https://gitlab.com/alline/hook-wiki/commit/6b0f522a199ada3eb8a914405760a86566cb5859))


### BREAKING CHANGES

* dropped support for @alline/scraper-html v1

## [1.1.2](https://gitlab.com/alline/hook-wiki/compare/v1.1.1...v1.1.2) (2020-06-29)


### Bug Fixes

* update dependencies ([72e64df](https://gitlab.com/alline/hook-wiki/commit/72e64dfad123060f88570e292ccf33c5ce0e2121))

## [1.1.1](https://gitlab.com/alline/hook-wiki/compare/v1.1.0...v1.1.1) (2020-06-29)


### Bug Fixes

* transformEpisodeHook return promise ([6e5ad63](https://gitlab.com/alline/hook-wiki/commit/6e5ad63f8ee4292e1ed543879a68f01eeb828417))

# [1.1.0](https://gitlab.com/alline/hook-wiki/compare/v1.0.0...v1.1.0) (2020-06-29)


### Features

* add validate function ([e71ab5a](https://gitlab.com/alline/hook-wiki/commit/e71ab5a0d8ff379cea2d4176f9968fb01d27a718))

# 1.0.0 (2020-06-29)


### Features

* initial commit ([1c48b76](https://gitlab.com/alline/hook-wiki/commit/1c48b76b33929b57a3a2d849fab8b098854a3882))
