import { Episode } from "@alline/model";
import { EpisodeContext } from "@alline/core";
import { RequestContext } from "@alline/scraper-request";
import { parseTable } from "@joshuaavalon/cheerio-table-parser";
import _ from "lodash";
import moment from "moment";
import $ from "cheerio";

export interface Option {
  id: string;
  offset?: number;
  multipleRow?: number;
  mapping?: Partial<{
    title: number;
    aired: number;
    directors: number;
    writers: number;
  }>;
  parsers?: Partial<{
    title: (elements: CheerioElement[]) => string[];
    aired: (elements: CheerioElement[]) => string;
    directors: (elements: CheerioElement[]) => string[];
    writers: (elements: CheerioElement[]) => string[];
  }>;
  validate?: (table: CheerioElement[][]) => void;
}

const mapMultipleRows = (
  table: CheerioElement[][],
  multipleRow?: number
): number[][] => {
  if (_.isUndefined(multipleRow)) {
    return _.range(table.length).map(i => [i]);
  }
  const rowMap: number[][] = [];
  let index = 0;
  let prev: CheerioElement | undefined = undefined;
  table.forEach((cols, row) => {
    if (!_.isArray(rowMap[index])) {
      rowMap[index] = [];
    }
    const current = cols[multipleRow];
    if (_.eq(current, prev) || row === 0) {
      rowMap[index].push(row);
    } else {
      index++;
      rowMap[index] = [row];
    }
    prev = current;
  });
  return rowMap;
};

const normalizeElement = (element: CheerioElement): void => {
  $("sup", element).remove();
  $("style", element).remove();
  $("ruby", element).each((_, ruby): void => {
    if ($("rp", ruby).length > 0) {
      return;
    }
    $("rt", ruby).prepend("(").append(")");
  });
  $("br", element).replaceWith("\n");
  $("hr", element).replaceWith("\n");
  $("li", element).append("\n");
};

const parseSimpleString = (elements: CheerioElement[]): string[] =>
  _.flatMap(elements, e => {
    normalizeElement(e);
    return $(e).text().replace(/\n+/g, " ");
  });

const parseSimpleDate = (values: CheerioElement[]): string =>
  moment($(values[0]).text()).format("YYYY-MM-DD");

const parseSpaceString = (elements: CheerioElement[]): string[] => {
  const result = _.flatMap(elements, e =>
    $(e)
      .text()
      .split(/\s+/)
      .filter(v => v)
  );
  return _.uniq(result);
};

const createData = (
  data: Episode,
  option: Option,
  table: CheerioElement[][],
  rowMap: number[]
): Episode => {
  const { mapping = {}, parsers = {} } = option;
  const {
    title: parseTitle = parseSimpleString,
    aired: parseAired = parseSimpleDate,
    directors: parseDirectors = parseSpaceString,
    writers: parseWriters = parseSpaceString
  } = parsers;
  const { title, aired, directors, writers } = mapping;
  const values: CheerioElement[][] = [];
  rowMap.forEach(row => {
    table[row].forEach((col, i) => {
      if (!_.isArray(values[i])) {
        values[i] = [];
      }
      values[i] = [...values[i], col];
    });
  });
  if (_.isNumber(title)) {
    data.title = parseTitle(values[title]);
  }
  if (_.isNumber(aired)) {
    data.aired = parseAired(values[aired]);
  }
  if (_.isNumber(directors)) {
    data.directors = parseDirectors(values[directors]);
  }
  if (_.isNumber(writers)) {
    data.writers = parseWriters(values[writers]);
  }
  return data;
};

export const transformEpisodeHook = (option: Option) => (
  data: Episode,
  ctx: RequestContext<CheerioStatic, string>
): Episode => {
  const { logger, value: dom } = ctx;
  const { id, offset = 0, multipleRow, validate } = option;
  const tables = dom(`#${id}`).parent().nextAll("table.wikitable").toArray();
  if (tables.length <= offset) {
    logger.error("Offset greater than table count.", {
      id,
      offset,
      tableCount: tables.length
    });
    throw new Error(`There are only ${tables.length} table(s).`);
  }
  const tableNode = tables[offset];
  const table = parseTable(tableNode, { parser: e => e });
  validate?.(table);
  const rowNum = ctx.episode;
  const rowMap = mapMultipleRows(table, multipleRow)[rowNum];
  return createData(data, option, table, rowMap);
};

export const transformUrlHook = (language: string, topic: string) => (
  _: string,
  ctx: EpisodeContext
): string => {
  const { logger } = ctx;
  const url = `https://${language}.wikipedia.org/wiki/${topic}`;
  logger.debug("wikiTransformUrlHook", { url });
  return url;
};

export const validateText = (cells: [number, number, string][]) => (
  table: CheerioElement[][]
): void => {
  cells.forEach(([row, col, except]) => {
    const value = $(table[row][col]).text();
    if (value !== except) {
      throw new Error(`Except "${value}" to be "${except}"`);
    }
  });
};
