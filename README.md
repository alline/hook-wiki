# @alline/hook-wiki

[![License][license_badge]][license] [![Pipelines][pipelines_badge]][pipelines] [![Coverage][coverage_badge]][pipelines] [![NPM][npm_badge]][npm] [![semantic-release][semantic_release_badge]][semantic_release]

Wikipedia hook for Alline.

## Installation

```
npm i @alline/hook-wiki
```

[license]: https://gitlab.com/alline/hook-wiki/blob/master/LICENSE
[license_badge]: https://img.shields.io/badge/license-Apache--2.0-green.svg
[pipelines]: https://gitlab.com/alline/hook-wiki/pipelines
[pipelines_badge]: https://gitlab.com/alline/hook-wiki/badges/master/pipeline.svg
[coverage_badge]: https://gitlab.com/alline/hook-wiki/badges/master/coverage.svg
[npm]: https://www.npmjs.com/package/@alline/hook-wiki
[npm_badge]: https://img.shields.io/npm/v/@alline/hook-wiki/latest.svg
[semantic_release]: https://github.com/semantic-release/semantic-release
[semantic_release_badge]: https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg
